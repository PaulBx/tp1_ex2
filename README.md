# Exercice 2 TP1

## Objectif
L'objectif de cet exercice est de synthétiser sous forme de classe SimpleCalculator, les quatres fonctions développées dans le premier exercice, c'est à dire les quatres opérations élementaires.

## Réalisation
Pour ce faire j'ai créé une classe SimpleCalculator qui possède quatres méthodes (1 méthode par opération) et qui possède un script de test appelé lorsqu'on appelle le script python dans un terminal

## Lancement
Pour lancer le test du programme il suffit de se placer dans le répertoire tp1_ex2 et de saisir la ligne de commande
`python exo_2.py`

