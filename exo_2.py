#!/usr/bin/env python
# coding : utf-8
""" 
author : paul bouyssoux

"""
class SimpleCalculator:
    def __init__(self, mon_entier_1, mon_entier_2):
        self.mon_entier_1 = mon_entier_1
        self.mon_entier_2 = mon_entier_2

    def fsum(self):
        return self.mon_entier_1 + self.mon_entier_2

    def substract(self):
        return self.mon_entier_1 - self.mon_entier_2

    def multiply(self):
        return self.mon_entier_1 * self.mon_entier_2

    def divide(self):
        if self.mon_entier_2 != 0:
            return self.mon_entier_1 / self.mon_entier_2
        return "vous avez essayé de diviser par zéro"


if __name__ == '__main__':
    MON_TEST = SimpleCalculator(10, 5)
    print("Test de la classe avec 10 et 5")
    print("somme : ", MON_TEST.fsum())
    print("soustraction : ", MON_TEST.substract())
    print("multiplication : ",MON_TEST.multiply())
    print("division : ", MON_TEST.divide())


    MON_TEST2 = SimpleCalculator(10, 0)
    print("Test de la classe avec 10 et 0")
    print("somme : ", MON_TEST2.fsum())
    print("soustraction : ", MON_TEST2.substract())
    print("multiplication : ", MON_TEST2.multiply())
    print("division : ", MON_TEST2.divide())
